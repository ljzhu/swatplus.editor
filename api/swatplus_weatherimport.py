from helpers.executable_api import ExecutableApi, Unbuffered
from database.project import base as project_base
from database.project.setup import SetupProjectDatabase
from database.project.config import Project_config
from database.project.climate import Weather_file, Weather_sta_cli
from database.project.connect import Aquifer_con, Channel_con, Rout_unit_con, Reservoir_con, Recall_con, Hru_con
from database import lib as db_lib

import sys
import os, os.path
import math
import argparse


HMD_TXT = "hmd.txt"
PCP_TXT = "pcp.txt"
SLR_TXT = "slr.txt"
TMP_TXT = "tmp.txt"
WND_TXT = "wnd.txt"

HMD_CLI = "hmd.cli"
PCP_CLI = "pcp.cli"
SLR_CLI = "slr.cli"
TMP_CLI = "tmp.cli"
WND_CLI = "wnd.cli"


class WeatherImport(ExecutableApi):
    def __init__(self, project_db_file, delete_existing):
        self.__abort = False
        SetupProjectDatabase.init(project_db_file)
        self.project_db = project_base.db

        if delete_existing:
            self.delete_existing()

    def import_data(self):
        try:
            config = Project_config.get()
            if config.weather_data_dir is None:
                sys.exit('Weather data directory not set in config table.')

            self.add_weather_files(config.weather_data_dir)
            self.create_weather_stations(20, 45)
            self.match_stations(65)
        except Project_config.DoesNotExist:
            sys.exit('Could not retrieve project configuration from database')

    def delete_existing(self):
        Weather_file.delete().execute()
        Weather_sta_cli.delete().execute()

    def match_stations(self, start_prog):
        self.match_stations_table(Aquifer_con, "aquifer connections", start_prog)
        self.match_stations_table(Channel_con, "channel connections", start_prog + 5)
        self.match_stations_table(Rout_unit_con, "routing unit connections", start_prog + 10)
        self.match_stations_table(Reservoir_con, "reservoir connections", start_prog + 15)
        self.match_stations_table(Recall_con, "recall connections", start_prog + 20)
        self.match_stations_table(Hru_con, "hru connections", start_prog + 25)

    def match_stations_table(self, table, name, prog):
        self.emit_progress(prog, "Adding weather stations to {name}...".format(name=name))
        for row in table.select():
            """
            See: http://stackoverflow.com/a/7472230
            For explanation of getting the closest lat,long
            """
            fudge = math.pow(math.cos(math.radians(row.lat)), 2)
            cursor = project_base.db.execute_sql("select id from weather_sta_cli order by ((? - lat) * (? - lat) + (? - lon) * (? - lon) * ?) limit 1", (row.lat, row.lat, row.long, row.long, fudge))
            res = cursor.fetchone()
            id = res[0]

            row.wst = id
            row.save()

    def create_weather_stations(self, start_prog, total_prog):  # total_prog is the total progress percentage available for this method
        if self.__abort: return

        stations = []
        cursor = project_base.db.execute_sql("select lat, lon from weather_file group by lat, lon")
        data = cursor.fetchall()
        records = len(data)
        i = 1
        for row in data:
            if self.__abort: return

            lat = row[0]
            lon = row[1]
            name = "w{lat}{lon}".format(lat=round(lat*1000), lon=round(lon*1000))

            prog = round(i * total_prog / records) + start_prog
            self.emit_progress(prog, "Creating weather station {name}...".format(name=name))

            station = {
                "name": name,
                "wnd_dir": "sim",
                "atmo_dep": "sim",
                "lat": lat,
                "lon": lon
            }

            for matching_file in Weather_file.select().where((Weather_file.lat == lat) & (Weather_file.lon == lon)):
                if matching_file.type == "hmd":
                    station["hmd"] = matching_file.filename
                elif matching_file.type == "pcp":
                    station["pcp"] = matching_file.filename
                elif matching_file.type == "slr":
                    station["slr"] = matching_file.filename
                elif matching_file.type == "tmp":
                    station["tmp"] = matching_file.filename
                elif matching_file.type == "wnd":
                    station["wnd"] = matching_file.filename

            stations.append(station)
            i += 1

        db_lib.bulk_insert(project_base.db, Weather_sta_cli, stations)

    def add_weather_files(self, dir):
        if self.__abort: return
        hmd_res = self.add_weather_files_type(os.path.join(dir, HMD_CLI), "hmd", 0)
        if self.__abort: return
        pcp_res = self.add_weather_files_type(os.path.join(dir, PCP_CLI), "pcp", 5)
        if self.__abort: return
        slr_res = self.add_weather_files_type(os.path.join(dir, SLR_CLI), "slr", 10)
        if self.__abort: return
        tmp_res = self.add_weather_files_type(os.path.join(dir, TMP_CLI), "tmp", 15)
        if self.__abort: return
        wnd_res = self.add_weather_files_type(os.path.join(dir, WND_CLI), "wnd", 20)

        if self.__abort: return
        warnings = []
        warnings.append(hmd_res)
        warnings.append(pcp_res)
        warnings.append(slr_res)
        warnings.append(tmp_res)
        warnings.append(wnd_res)
        has_warnings = any(x is not None for x in warnings)

        if has_warnings:
            with open(os.path.join( dir, "__warnings.txt"), 'w+') as warning_file:
                for w in warnings:
                    if w is not None:
                        warning_file.write(w)
                        warning_file.write("\n")

    def add_weather_files_type(self, source_file, weather_type, prog):
        if not os.path.exists(source_file):
            return "Skipping {type} import. File does not exist: {file}".format(type=weather_type, file=source_file)
        else:
            self.emit_progress(prog, "Inserting {type} files and coordinates...".format(type=weather_type))
            weather_files = []
            dir = os.path.dirname(source_file)
            with open(source_file, "r") as source_data:
                i = 0
                for line in source_data:
                    if self.__abort:
                        break

                    if i > 1:
                        station_name = line.strip('\n')
                        station_file = os.path.join(dir, station_name)
                        if not os.path.exists(station_file):
                            raise IOError("File {file} not found. Weather data import aborted.".format(file=station_file))

                        with open(station_file, "r") as station_data:
                            j = 0
                            for sline in station_data:
                                if j == 2:
                                    station_info = sline.strip().split()
                                    if len(station_info) < 4:
                                        raise ValueError("Invalid value at line {ln} of {file}. Expecting nbyr, tstep, lat, long, elev values separated by a space.".format(ln=str(j + 1), file=station_file))

                                    lat = float(station_info[2])
                                    lon = float(station_info[3])

                                    file = {
                                        "filename": station_name,
                                        "type": weather_type,
                                        "lat": lat,
                                        "lon": lon
                                    }
                                    weather_files.append(file)
                                elif j > 2:
                                    break

                                j += 1

                    i += 1

            db_lib.bulk_insert(project_base.db, Weather_file, weather_files)


if __name__ == '__main__':
    sys.stdout = Unbuffered(sys.stdout)
    parser = argparse.ArgumentParser(description="Import SWAT+ weather files into project SQLite database.")
    parser.add_argument("project_db_file", type=str, help="full path of project SQLite database file")
    parser.add_argument("delete_existing", type=str, help="y/n delete existing data first")
    args = parser.parse_args()

    del_ex = True if args.delete_existing == "y" else False

    weather_api = WeatherImport(args.project_db_file, del_ex)
    weather_api.import_data()
