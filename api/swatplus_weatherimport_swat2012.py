from helpers.executable_api import ExecutableApi, Unbuffered
from database.project import base as project_base
from database.project.setup import SetupProjectDatabase
from database.project.config import Project_config
import swatplus_weatherimport as weather_import

import sys
import os, os.path
import argparse
import time, datetime


WEATHER_DESC = {
    "hmd": "Relative humidity",
    "pcp": "Precipitation",
    "slr": "Solar radiation",
    "tmp": "Temperature",
    "wnd": "Wind speed"
}


class Swat2012WeatherImport(ExecutableApi):
    def __init__(self, project_db_file, delete_existing, source_dir):
        self.__abort = False
        SetupProjectDatabase.init(project_db_file)
        config = Project_config.get()
        if config.weather_data_dir is None:
            sys.exit('Weather data directory not set in config table.')

        self.output_dir = config.weather_data_dir
        self.project_db_file = project_db_file
        self.project_db = project_base.db
        self.source_dir = source_dir
        self.delete_existing = delete_existing

    def import_data(self):
        try:
            self.write_to_swatplus(self.source_dir)
            weather_api = weather_import.WeatherImport(self.project_db_file, self.delete_existing)
            weather_api.import_data()
        except Project_config.DoesNotExist:
            sys.exit('Could not retrieve project configuration from database')

    def write_to_swatplus(self, dir):
        warnings = []

        if not os.path.exists(self.output_dir):
            os.makedirs(self.output_dir)

        total_files = len(os.listdir(dir))

        if self.__abort: return
        hmd_res = self.write_weather(os.path.join(dir, weather_import.HMD_TXT), os.path.join(self.output_dir, weather_import.HMD_CLI), "hmd", 1, total_files)
        if self.__abort: return
        pcp_res = self.write_weather(os.path.join(dir, weather_import.PCP_TXT), os.path.join(self.output_dir, weather_import.PCP_CLI), "pcp", hmd_res[0], total_files)
        if self.__abort: return
        slr_res = self.write_weather(os.path.join(dir, weather_import.SLR_TXT), os.path.join(self.output_dir, weather_import.SLR_CLI), "slr", pcp_res[0], total_files)
        if self.__abort: return
        tmp_res = self.write_weather(os.path.join(dir, weather_import.TMP_TXT), os.path.join(self.output_dir, weather_import.TMP_CLI), "tmp", slr_res[0], total_files)
        if self.__abort: return
        wnd_res = self.write_weather(os.path.join(dir, weather_import.WND_TXT), os.path.join(self.output_dir, weather_import.WND_CLI), "wnd", tmp_res[0], total_files)

        if self.__abort: return
        warnings.append(hmd_res[1])
        warnings.append(pcp_res[1])
        warnings.append(slr_res[1])
        warnings.append(tmp_res[1])
        warnings.append(wnd_res[1])
        has_warnings = any(x is not None for x in warnings)

        if has_warnings:
            with open(os.path.join(self.output_dir, "__warnings.txt"), 'w+') as warning_file:
                for w in warnings:
                    if w is not None:
                        warning_file.write(w)
                        warning_file.write("\n")

    def write_weather(self, source_file, dest_file, weather_type, starting_file_num, total_files):
        if not os.path.exists(source_file):
            return starting_file_num, "Skipping {type} import. File does not exist: {file}".format(type=weather_type, file=source_file)
        else:
            with open(dest_file, 'w+') as new_file:
                new_file.write("{file}.cli: {desc} file names - file written by SWAT+ editor {today}\n".format(file=weather_type, desc=WEATHER_DESC[weather_type], today=datetime.datetime.now()))
                new_file.write("FILENAME\n")

                with open(source_file, "r") as source_data:
                    i = 0
                    curr_file_num = starting_file_num
                    for line in source_data:
                        if self.__abort:
                            break

                        if i == 0 and not "ID,NAME,LAT,LONG,ELEVATION" in line:
                            return curr_file_num, "Skipping {type} import. Invalid file format in header: {file}. Expecting 'ID,NAME,LAT,LONG,ELEVATION'".format(type=weather_type, file=source_file)
                        if i > 0:
                            station_obj = [x.strip() for x in line.split(',')]
                            if len(station_obj) != 5:
                                return curr_file_num, "Skipping {type} import. Invalid file format in line {line_no}: {file}, {line}".format(type=weather_type, line_no=i+1, file=source_file, line=line)

                            new_file_name = "{s}.{ext}".format(s=station_obj[1], ext=weather_type)
                            new_file.write(new_file_name)
                            new_file.write("\n")

                            self.write_station(os.path.dirname(source_file), station_obj, weather_type)
                            prog = round(curr_file_num * 100 / total_files)
                            self.emit_progress(prog, "Writing {type}, {file}...".format(type=weather_type, file=new_file_name))
                            curr_file_num += 1

                        i += 1

            return curr_file_num, None

    def write_station(self, dir, station_obj, weather_type):
        source_file = os.path.join(dir, "{s}.txt".format(s=station_obj[1]))
        if not os.path.exists(source_file):
            return "Skipping {type} import. Station file does not exist: {file}".format(type=weather_type, file=source_file)

        dest_file_name = "{s}.{ext}".format(s=station_obj[1], ext=weather_type)
        dest_file = os.path.join(self.output_dir, dest_file_name)

        with open(dest_file, 'w+') as new_file:
            new_file.write("{file}: {desc} data - file written by SWAT+ editor {today}\n".format(file=dest_file_name, desc=WEATHER_DESC[weather_type], today=datetime.datetime.now()))
            new_file.write("NBYR".rjust(4))
            new_file.write("TSTEP".rjust(10))
            new_file.write("LAT".rjust(10))
            new_file.write("LONG".rjust(10))
            new_file.write("ELEV".rjust(10))
            new_file.write("\n")

            linecount = self.file_len(source_file)
            total_days = linecount - 1 if linecount > 0 else 0

            with open(source_file, "r") as station_file:
                i = 0
                date = None
                for line in station_file:
                    if i == 0:
                        ts = time.strptime(line.strip(), "%Y%m%d")
                        date = datetime.datetime(ts.tm_year, ts.tm_mon, ts.tm_mday)
                        start_date = date

                        end_date = start_date + datetime.timedelta(days=total_days)
                        nbyr = end_date.year - start_date.year + 1

                        new_file.write(str(nbyr).rjust(4))
                        new_file.write("0".rjust(10))
                        new_file.write("{0:.3f}".format(float(station_obj[2])).rjust(10))
                        new_file.write("{0:.3f}".format(float(station_obj[3])).rjust(10))
                        new_file.write("{0:.3f}".format(float(station_obj[4])).rjust(10))
                        new_file.write("\n")
                    else:
                        day_of_year = date.timetuple().tm_yday

                        new_file.write(str(date.year))
                        new_file.write(str(day_of_year).rjust(5))

                        if weather_type == "tmp":
                            tmp = [x.strip() for x in line.split(',')]
                            new_file.write(tmp[0].rjust(10))
                            new_file.write(tmp[1].rjust(10))
                            new_file.write("\n")
                        else:
                            new_file.write(line.rjust(10))

                        date = date + datetime.timedelta(days=1)

                    i += 1

    def file_len(self, fname):
        with open(fname) as f:
            for i, l in enumerate(f):
                pass
        return i + 1


if __name__ == '__main__':
    sys.stdout = Unbuffered(sys.stdout)
    parser = argparse.ArgumentParser(description="Import SWAT+ weather files into project SQLite database.")
    parser.add_argument("source_dir", type=str, help="full path of SWAT2012 weather files")
    parser.add_argument("project_db_file", type=str, help="full path of project SQLite database file")
    parser.add_argument("delete_existing", type=str, help="y/n delete existing data first")
    args = parser.parse_args()

    del_ex = True if args.delete_existing == "y" else False

    weather_api = Swat2012WeatherImport(args.project_db_file, del_ex, args.source_dir)
    weather_api.import_data()
