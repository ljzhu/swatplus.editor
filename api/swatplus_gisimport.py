from database import lib as db_lib
from database.project import base as project_base, gis, routing_unit, channel, connect, aquifer, hydrology, hru, reservoir, soils, init, lum, parm_db
from database.project.setup import SetupProjectDatabase


class GisImport:
    def __init__(self, project_db_file):
        SetupProjectDatabase.init(project_db_file)
        self.project_db = project_base.db

    def insert_routing_units(self):
        """
        Insert routing unit, channel, and aquifer SWAT+ tables from GIS database.
        """
        if routing_unit.Rout_unit_rtu.select().count() > 0:
            raise ValueError('Your project database already has data imported from GIS.')

        hydrology_chas = []
        channel_chas = []
        channel_cons = []
        channel_con_outs = []

        aquifer_aqus = []
        aquifer_cons = []
        aquifer_con_outs = []

        topography = []
        rout_units = []
        rout_unit_cons = []
        rout_unit_con_outs = []
