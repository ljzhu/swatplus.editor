from flask import Flask
from flask_restful import Resource, Api

from flask_resources.setup import SetupApi, InputFilesSettingsApi
from flask_resources.simulation import TimeSimApi
from flask_resources import climate


app = Flask(__name__)
api = Api(app)


class SwatPlusConfig:
    VERSION = "1.0.0.dev1"
    SUPPORT_EMAIL = "eco.web@tamu.edu"


class SwatPlusApi(Resource):
    def get(self):
        return {'SWATPlusEditor': SwatPlusConfig.VERSION}


api.add_resource(SwatPlusApi, '/')
api.add_resource(SetupApi, '/setup')
api.add_resource(TimeSimApi, '/sim/time/<project_db>')

api.add_resource(climate.WeatherStationListApi, '/climate/stations/<project_db>')
api.add_resource(climate.WeatherStationApi, '/climate/stations/<project_db>/<id>')
api.add_resource(climate.WeatherStationPageApi, '/climate/stations/<project_db>/<sort>/<reverse>/<page>/<items_per_page>')
api.add_resource(climate.WeatherStationSaveDirApi, '/climate/stations/directory/<project_db>')
api.add_resource(climate.WeatherFileAutoCompleteApi, '/climate/stations/files/<project_db>/<type>/<partial_name>')

api.add_resource(climate.WgnListApi, '/climate/wgn/<project_db>')
api.add_resource(climate.WgnApi, '/climate/wgn/<project_db>/<id>')
api.add_resource(climate.WgnPageApi, '/climate/wgn/<project_db>/<sort>/<reverse>/<page>/<items_per_page>')
api.add_resource(climate.WgnSaveImportDbApi, '/climate/wgn/db/<project_db>')
api.add_resource(climate.WgnAutoCompleteApi, '/climate/wgn/autocomplete/<project_db>/<partial_name>')

api.add_resource(climate.WgnMonthListApi, '/climate/wgn/months/<project_db>/<wgn_id>')
api.add_resource(climate.WgnMonthApi, '/climate/wgn/month/<project_db>/<id>')

api.add_resource(InputFilesSettingsApi, '/setup/inputfiles/<project_db>')


if __name__ == '__main__':
    app.run()