from helpers.executable_api import ExecutableApi, Unbuffered
from database.project import base as project_base
from database.project.setup import SetupProjectDatabase
from database.project.config import Project_config
from database.project.climate import Weather_sta_cli, Weather_wgn_cli, Weather_wgn_cli_mon
from database.project.connect import Aquifer_con, Channel_con, Rout_unit_con, Reservoir_con, Recall_con, Hru_con
from database import lib as db_lib

import sys
import os, os.path
import math
import argparse
import sqlite3
from peewee import *


class WgnImport(ExecutableApi):
    def __init__(self, project_db_file, delete_existing):
        self.__abort = False
        SetupProjectDatabase.init(project_db_file)
        self.project_db = project_base.db

        try:
            config = Project_config.get()
            if config.wgn_db is None:
                sys.exit('Weather generator database not set in config table.')
            if config.wgn_table_name is None:
                sys.exit('Weather generator table name not set in config table.')

            self.wgn_database = config.wgn_db
            self.wgn_table = config.wgn_table_name
        except Project_config.DoesNotExist:
            sys.exit('Could not retrieve project configuration from database')

        if delete_existing:
            self.delete_existing()

    def import_data(self):
        #self.add_wgn_stations(0, 70)
        self.match_to_weather_stations(70, 30)

    def delete_existing(self):
        Weather_wgn_cli_mon.delete().execute()
        Weather_wgn_cli.delete().execute()

    def add_wgn_stations(self, start_prog, total_prog):
        if self.__abort: return
        conn = sqlite3.connect(self.wgn_database)
        conn.row_factory = sqlite3.Row

        monthly_table = "{}_mon".format(self.wgn_table)

        if not db_lib.exists_table(conn, self.wgn_table):
            raise ValueError(
                "Table {table} does not exist in {file}.".format(table=self.wgn_table, file=self.wgn_database))

        if not db_lib.exists_table(conn, monthly_table):
            raise ValueError(
                "Table {table} does not exist in {file}.".format(table=monthly_table, file=self.wgn_database))

        if Rout_unit_con.select().count() > 0:
            coords = Rout_unit_con.select(fn.Min(Rout_unit_con.lat).alias("min_lat"),
                                          fn.Max(Rout_unit_con.lat).alias("max_lat"),
                                          fn.Min(Rout_unit_con.lon).alias("min_lon"),
                                          fn.Max(Rout_unit_con.lon).alias("max_lon")
                                          ).get()

            query = "select * from {table_name} where lat between ? and ? and lon between ? and ? order by name".format(table_name=self.wgn_table)
            cursor = conn.cursor().execute(query, (coords.min_lat, coords.max_lat, coords.min_lon, coords.max_lon))
        else:
            query = "select * from {table_name} order by name".format(table_name=self.wgn_table)
            cursor = conn.cursor().execute(query)

        wgns = []
        ids = []

        data = cursor.fetchall()
        records = len(data)

        i = 1
        for row in data:
            if self.__abort: return

            prog = round(i * (total_prog / 2) / records) + start_prog
            self.emit_progress(prog, "Preparing weather generator {name}...".format(name=row['name']))
            i += 1

            ids.append(row['id'])
            wgn = {
                "id": row['id'],
                "name": row['name'],
                "lat": row['lat'],
                "lon": row['lon'],
                "elev": row['elev'],
                "rain_yrs": row['rain_yrs']
            }
            wgns.append(wgn)

        prog = round(i * (total_prog / 2) / records) + start_prog
        self.emit_progress(prog, "Inserting {total} weather generators...".format(total=len(ids)))
        db_lib.bulk_insert(project_base.db, Weather_wgn_cli, wgns)

        # Chunk the id array so we don't hit the SQLite parameter limit!
        max_length = 999
        id_chunks = [ids[i:i + max_length] for i in range(0, len(ids), max_length)]

        i = 1
        start_prog = start_prog + (total_prog / 2)

        mon_count_query = "select count(*) from {table_name}".format(table_name=monthly_table)
        total_mon_rows = conn.cursor().execute(mon_count_query).fetchone()[0]
        current_total = 0

        for chunk in id_chunks:
            monthly_values = []
            mon_query = "select * from {table_name} where wgn_id in ({ids})".format(table_name=monthly_table, ids=",".join('?'*len(chunk)))
            mon_cursor = conn.cursor().execute(mon_query, chunk)
            mon_data = mon_cursor.fetchall()
            mon_records = len(mon_data)
            i = 1

            for row in mon_data:
                if self.__abort: return

                if i == 1 or (i % 12 == 0):
                    prog = round(i * (total_prog / 2) / mon_records) + start_prog
                    self.emit_progress(prog, "Preparing monthly values {i}/{total}...".format(i=i, total=mon_records))
                i += 1

                mon = {
                    "weather_wgn_cli": row['wgn_id'],
                    "month": row['month'],
                    "tmp_max_ave": row['tmp_max_ave'],
                    "tmp_min_ave": row['tmp_min_ave'],
                    "tmp_max_sd": row['tmp_max_sd'],
                    "tmp_min_sd": row['tmp_min_sd'],
                    "pcp_ave": row['pcp_ave'],
                    "pcp_sd": row['pcp_sd'],
                    "pcp_skew": row['pcp_skew'],
                    "wet_dry": row['wet_dry'],
                    "wet_wet": row['wet_wet'],
                    "pcp_days": row['pcp_days'],
                    "pcp_hhr": row['pcp_hhr'],
                    "slr_ave": row['slr_ave'],
                    "dew_ave": row['dew_ave'],
                    "wnd_ave": row['wnd_ave']
                }
                monthly_values.append(mon)

            prog = round(i * (total_prog / 2) / mon_records) + start_prog
            current_total = current_total + mon_records
            self.emit_progress(prog, "Inserting monthly values {rec}/{total}...".format(rec=current_total, total=total_mon_rows))
            db_lib.bulk_insert(project_base.db, Weather_wgn_cli_mon, monthly_values)

    def match_to_weather_stations(self, start_prog, total_prog):
        query = Weather_sta_cli.select()
        records = query.count()
        i = 1
        for row in query:
            if self.__abort: return

            prog = round(i * total_prog / records) + start_prog
            i += 1

            if row.lat is not None and row.lon is not None:
                """
                See: http://stackoverflow.com/a/7472230
                For explanation of getting the closest lat,long
                """
                fudge = math.pow(math.cos(math.radians(row.lat)), 2)
                cursor = project_base.db.execute_sql("select id from weather_wgn_cli order by ((? - lat) * (? - lat) + (? - lon) * (? - lon) * ?) limit 1", (row.lat, row.lat, row.lon, row.lon, fudge))
                res = cursor.fetchone()
                id = res[0]

                self.emit_progress(prog, "Updating weather station with generator {i}/{total}...".format(i=i, total=records))
                row.wgn_id = id
                row.save()


if __name__ == '__main__':
    sys.stdout = Unbuffered(sys.stdout)
    parser = argparse.ArgumentParser(description="Import weather generator data into project SQLite database.")
    parser.add_argument("project_db_file", type=str, help="full path of project SQLite database file")
    parser.add_argument("delete_existing", type=str, help="y/n delete existing data first")
    args = parser.parse_args()

    del_ex = True if args.delete_existing == "y" else False

    api = WgnImport(args.project_db_file, del_ex)
    api.import_data()
