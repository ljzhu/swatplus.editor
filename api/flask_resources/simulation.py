from flask_restful import Resource, reqparse, abort
from playhouse.shortcuts import model_to_dict

from database.project.setup import SetupProjectDatabase
from database.project.simulation import Time_sim


class TimeSimApi(Resource):
    def get(self, project_db):
        SetupProjectDatabase.init(project_db)
        m = Time_sim.get_or_create_default()
        return model_to_dict(m)

    def put(self, project_db):
        parser = reqparse.RequestParser()
        parser.add_argument('id', type=int, required=False, location='json')
        parser.add_argument('jd_start', type=int, required=True, location='json')
        parser.add_argument('yr_start', type=int, required=True, location='json')
        parser.add_argument('jd_end', type=int, required=True, location='json')
        parser.add_argument('yr_end', type=int, required=True, location='json')
        parser.add_argument('step', type=int, required=False, location='json')
        args = parser.parse_args(strict=True)

        SetupProjectDatabase.init(project_db)
        result = Time_sim.update_and_exec(args['jd_start'], args['yr_start'], args['jd_end'], args['yr_end'], args['step'])

        if result == 1:
            return 200

        abort(400, message='Unable to update time_sim table.')
