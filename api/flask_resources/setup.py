from flask_restful import Resource, reqparse, abort
from playhouse.shortcuts import model_to_dict

from database.project.config import Project_config
from database.project.setup import SetupProjectDatabase


class SetupApi(Resource):
    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('project_name', type=str, required=True, location='json')
        parser.add_argument('editor_version', type=str, required=True, location='json')
        parser.add_argument('project_db', type=str, required=True, location='json')
        parser.add_argument('reference_db', type=str, required=True, location='json')
        parser.add_argument('settings', type=str, required=False, location='json')
        args = parser.parse_args(strict=True)

        SetupProjectDatabase.init(args['project_db'], args['reference_db'])
        SetupProjectDatabase.create_tables()
        SetupProjectDatabase.copy_default_datasets()
        config = Project_config.get_or_create_default(editor_version=args['editor_version'], project_name=args['project_name'])

        return model_to_dict(config), 201


class InputFilesSettingsApi(Resource):
    def get(self, project_db):
        SetupProjectDatabase.init(project_db)
        try:
            m = Project_config.get()
            return {"input_files_dir": m.input_files_dir, "input_files_last_written": m.input_files_last_written}
        except Project_config.DoesNotExist:
            abort(404, message="Could not retrieve project configuration data.")

    def put(self, project_db):
        parser = reqparse.RequestParser()
        parser.add_argument('input_files_dir', type=str, required=False, location='json')
        parser.add_argument('input_files_last_written', type=str, required=False, location='json')
        args = parser.parse_args(strict=True)

        SetupProjectDatabase.init(project_db)
        try:
            m = Project_config.get()
            m.input_files_dir = args['input_files_dir']
            m.input_files_last_written = args['input_files_last_written']
            result = m.save()

            if result > 0:
                return 200

            abort(400, message="Unable to update project configuration table.")
        except Project_config.DoesNotExist:
            abort(404, message="Could not retrieve project configuration data.")
