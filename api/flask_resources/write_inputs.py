from flask_restful import Resource, reqparse, abort
from playhouse.shortcuts import model_to_dict

from database.project.setup import SetupProjectDatabase
from database.project.config import Project_config


class InputFilesSettingsApi(Resource):
    def get(self, project_db):
        SetupProjectDatabase.init(project_db)
        try:
            m = Project_config.get()
            return {"input_files_dir": m.input_files_dir, "input_files_last_written": m.input_files_last_written}
        except Project_config.DoesNotExist:
            abort(404, message="Could not retrieve project configuration data.")

    def put(self, project_db):
        parser = reqparse.RequestParser()
        parser.add_argument('input_files_dir', type=str, required=True, location='json')
        parser.add_argument('input_files_last_written', type=str, required=True, location='json')
        args = parser.parse_args(strict=True)

        SetupProjectDatabase.init(project_db)
        try:
            m = Project_config.get()
            m.input_files_dir = args['input_files_dir']
            m.input_files_last_written = args['input_files_last_written']
            result = m.save()

            if result > 0:
                return 200

            abort(400, message="Unable to update project configuration table.")
        except Project_config.DoesNotExist:
            abort(404, message="Could not retrieve project configuration data.")
