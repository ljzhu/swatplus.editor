from peewee import *
from . import base


class Constituents_cs(base.BaseModel):
    name = CharField()
    units = IntegerField()
    pest_com = CharField(null=True)
    pest_dat = CharField(null=True)
    path_com = CharField(null=True)
    path_dat = CharField(null=True)
    hmet_com = CharField(null=True)
    hmet_dat = CharField(null=True)
    salt_com = CharField(null=True)
    salt_dat = CharField(null=True)


class Community_cs(base.BaseModel):
    name = CharField()
    init = CharField(null=True)
    rec = CharField(null=True)
    exco = CharField(null=True)
    dlr = CharField(null=True)
    cnt = IntegerField()


""" 
Documentation not fully complete for these.
Currently used text fields below to contain a list of names. This seems easier if it is indeed just names.
Alternatively could use a related table; but might be overkill.
"""
class Pest_cs(Community_cs):
    pests = TextField(null=True)


class Path_cs(Community_cs):
    paths = TextField(null=True)


class Hmet_cs(Community_cs):
    metals = TextField(null=True)


class Salt_cs(Community_cs):
    salts = TextField(null=True)
