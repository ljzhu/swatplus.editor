from peewee import *
from . import base


class Gis_subbasins(base.BaseModel):
    area = DoubleField()
    slo1 = DoubleField()
    len1 = DoubleField()
    sll = DoubleField()
    lat = DoubleField()
    lon = DoubleField()
    elev = DoubleField()
    elevmin = DoubleField()
    elevmax = DoubleField()


class Gis_channels(base.BaseModel):
    subbasin = IntegerField()
    areac = DoubleField()
    len2 = DoubleField()
    slo2 = DoubleField()
    wid2 = DoubleField()
    dep2 = DoubleField()
    elevmin = DoubleField()
    elevmax = DoubleField()


class Gis_lsus(base.BaseModel):
    category = IntegerField()
    channel = IntegerField()
    area = DoubleField()
    slope = DoubleField()
    csl = DoubleField()
    wid1 = DoubleField()
    dep1 = DoubleField()
    lat = DoubleField()
    lon = DoubleField()
    elev = DoubleField()


class Gis_hrus(base.BaseModel):
    lsu = IntegerField()
    relid = IntegerField()
    arsub = DoubleField()
    arlsu = DoubleField()
    landuse = DoubleField()
    arland = DoubleField()
    soil = DoubleField()
    arso = DoubleField()
    slp = DoubleField()
    arslp = DoubleField()
    slope = DoubleField()
    lat = DoubleField()
    lon = DoubleField()
    elev = DoubleField()


class Gis_water(base.BaseModel):
    wtype = CharField()
    lsu = IntegerField()
    area = DoubleField()
    lat = DoubleField()
    lon = DoubleField()
    elev = DoubleField()


class Gis_points(base.BaseModel):
    subbasin = IntegerField()
    ptype = CharField()
    xpr = DoubleField()
    ypr = DoubleField()
    lat = DoubleField()
    lon = DoubleField()
    elev = DoubleField()


class Gis_routing(base.BaseModel):
    sourceid = PrimaryKeyField()
    sourcecat = CharField()
    sinkid = IntegerField()
    sinkcat = CharField()
    percent = DoubleField()
