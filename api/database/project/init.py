from peewee import *
from . import base
from . import parm_db

class Initial_plt(base.BaseModel):
    name = CharField()
    description = TextField(null=True)

class Initial_plt_item(base.BaseModel):
    initial_plt = ForeignKeyField(Initial_plt, on_delete = 'CASCADE', related_name="plants")
    plant_name = ForeignKeyField(parm_db.Plants_plt, on_delete = 'CASCADE', null=True)
    lc_status = IntegerField()
    lai_init = DoubleField()
    bm_init = DoubleField()
    phu_init = DoubleField()
    plant_pop = DoubleField()
    yrs_init = DoubleField()
    rsd_init = DoubleField()

class Initial_pst(base.BaseModel): # Looks incomplete in spreadsheet
    name = CharField()
    pest_cnt = IntegerField()
    exco_df = DoubleField()
    del_df = DoubleField()
    pest_name = CharField()
    pest_plant = DoubleField()
    pest_soil = DoubleField()
    enrich = DoubleField()