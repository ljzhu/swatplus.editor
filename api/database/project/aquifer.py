from peewee import *
from . import base


class Aquifer_aqu(base.BaseModel):
    gw_flo = DoubleField()
    gw_dp = DoubleField()
    gw_ht = DoubleField()
    no3_n = DoubleField()
    sol_p = DoubleField()
    ptl_n = DoubleField()
    ptl_p = DoubleField()
    delay = DoubleField()
    alpha_bf = DoubleField()
    revap = DoubleField()
    rchg_dp = DoubleField()
    spec_yld = DoubleField()
    hl_no3n = DoubleField()
    flo_min = DoubleField()
    revap_min = DoubleField()
