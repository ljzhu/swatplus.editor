from peewee import *
from . import base


class Initial_res(base.BaseModel):
    name = CharField()
    vol = DoubleField()
    sed = DoubleField()
    ptl_n = DoubleField()
    no3_n = DoubleField()
    no2_n = DoubleField()
    nh3_n = DoubleField()
    ptl_p = DoubleField()
    so_lp = DoubleField()
    secchii = DoubleField()
    sand = DoubleField()
    silt = DoubleField()
    clay = DoubleField()
    sm_agg = DoubleField()
    lg_agg = DoubleField()
    gravel = DoubleField()
    chla = DoubleField()
    sol_pest = DoubleField()
    srb_pest = DoubleField()
    lp_bact = DoubleField()
    p_bact = DoubleField()
    description = TextField(null=True)


class Hydrology_res(base.BaseModel):
    name = CharField()
    yr_op = IntegerField()
    mon_op = IntegerField()
    area_ps = DoubleField()
    vol_ps = DoubleField()
    area_es = DoubleField()
    vol_es = DoubleField()
    k = DoubleField()
    evap_co = DoubleField()
    shp_co1 = DoubleField()
    shp_co2 = DoubleField()


class Nutrients_res(base.BaseModel):
    name = CharField()
    mid_start = IntegerField()
    mid_end = IntegerField()
    mid_n_stl = DoubleField()
    n_stl = DoubleField()
    mid_p_stl = DoubleField()
    p_stl = DoubleField()
    chla_co = DoubleField()
    secchi_co = DoubleField()
    theta_n = DoubleField()
    theta_p = DoubleField()
    n_min_stl = DoubleField()
    p_min_stl = DoubleField()


class Pesticide_res(base.BaseModel):
    name = CharField()
    pest_conc = DoubleField()
    part_co = DoubleField()
    mix_vel = DoubleField()
    react_co = DoubleField()
    rsp_vel = DoubleField()
    stl_vel = DoubleField()
    volat_co = DoubleField()
    sed_dp = DoubleField()
    sed_bury = DoubleField()
    sed_conc = DoubleField()
    sed_react = DoubleField()


class Sediment_res(base.BaseModel):
    name = CharField()
    sed_amt = DoubleField()
    d50 = DoubleField()
    sed_stl = DoubleField()
    stl_vel = DoubleField()


class Weir_res(base.BaseModel):
    name = CharField()
    num_steps = DoubleField()
    disch_co = DoubleField()
    energy_co = DoubleField()
    weir_wd = DoubleField()
    vel_co = DoubleField()
    dp_co = DoubleField()


class Reservoir_res(base.BaseModel):
    name = CharField()
    init = ForeignKeyField(Initial_res, null=True)
    hyd = ForeignKeyField(Hydrology_res, null=True)
    rel = IntegerField()
    sed = ForeignKeyField(Sediment_res, null=True)
    nut = ForeignKeyField(Nutrients_res, null=True)
    pest = ForeignKeyField(Pesticide_res, null=True)
    description = TextField(null=True)


class Hydrology_wet(base.BaseModel):
    name = CharField()
    hru_ps = DoubleField()
    dp_ps = DoubleField()
    hru_es = DoubleField()
    dp_es = DoubleField()
    k = DoubleField()
    evap = DoubleField()
    vol_area_co = DoubleField()
    vol_dp_co = DoubleField()
    vol_dp_co2 = DoubleField()  #Typo in spreadsheet, named the same as above...
    hru_frac = DoubleField()


class Wetland_wet(base.BaseModel):
    name = CharField()
    init = ForeignKeyField(Initial_res, null=True)
    hyd = ForeignKeyField(Hydrology_res, null=True)
    rel = IntegerField()
    sed = ForeignKeyField(Sediment_res, null=True)
    nut = ForeignKeyField(Nutrients_res, null=True)
    pest = ForeignKeyField(Pesticide_res, null=True)
    description = TextField(null=True)
