How to Use SQLite
=================

As mentioned in the :ref:`swatplus-editor-design` doc, SWAT+ Editor uses uses a `SQLite <https://www.sqlite.org/>`_
database to hold model input data to allow easy manipulation by the user. The database is structured to closely
resemble the SWAT+ ASCII text files in order to keep a clean link between the model and editor.

Opening the SQLite Database
---------------------------

We recommend users stick to the SWAT+ Editor program provided to browse and edit SWAT+ input data.
However, if you need to access the database, we recommend using:

* `SQLite Studio <https://sqlitestudio.pl>`_

There are many other alternatives out there. A few of them are:

* `DB Browser for SQLite <http://sqlitebrowser.org/>`_
* `SQLite Manager, Firefox Add-On <https://addons.mozilla.org/en-US/firefox/addon/sqlite-manager/>`_

Understanding Table Relationships
---------------------------------

SWAT+ contains many links between files, and the database follows suit by creating foreign key relationships
where applicable. In the SWAT+ text files, you will see links reference object names. In the database however,
these are done with an integer id. Luckily, relational databases make it easy to view the referenced row.

In SQLite Studio, simply right-click a foreign key id in a row of data, and select "Go to referenced row in table ..."
as shown in the image below. This will open the referenced row of data in a new tab.

.. image:: foreign-key-link.png
    :align: center
    :alt: SQLite Studio foreign key link example image

Results in:

.. image:: foreign-key-results.png
    :align: center
    :alt: SQLite Studio reference row results example image
