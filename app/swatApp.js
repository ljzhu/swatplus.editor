var swatApp = angular.module('swatApp', [
	'ui.router', 'ui.bootstrap', require('angular-messages'),
	'swatApp.helpers',
	'swatApp.controllers',
	'swatApp.projectSetup.service',
	'swatApp.simulation.service',
	'swatApp.climate.service'
]);

swatApp.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
	$urlRouterProvider.otherwise('/project-setup');

	$stateProvider
		.state('help', {
			url: '/help',
			templateUrl: 'app/views/help.html',
			controller: 'HelpController'
		})
		.state('projectSetup', {
			url: '/project-setup',
			templateUrl: 'app/views/project-setup.html',
			controller: 'ProjectSetupController'
		})
		.state('writeInputs', {
			url: '/write-inputs',
			templateUrl: 'app/views/write-inputs.html',
			controller: 'WriteInputsController'
		})
		.state('runSwat', {
			url: '/run-swat',
			templateUrl: 'app/views/run-swat.html',
			controller: 'RunSwatController'
		})
		.state('output', {
			url: '/output',
			templateUrl: 'app/views/output.html',
			controller: 'OutputController'
		})
		.state('edit', {
			url: '/edit',
			templateUrl: 'app/views/edit/template.html',
			controller: 'EditTemplateController'
		})
			.state('edit.simTime', {
				url: '/sim-time',
				templateUrl: 'app/views/edit/sim-time.html',
				controller: 'SimTimeController'
			})
			.state('edit.simPrint', {
				url: '/sim-print',
				templateUrl: 'app/views/edit/sim-print.html'
			})
			.state('edit.climateStations', {
				url: '/climate-stations',
				templateUrl: 'app/views/edit/climate/stations.html',
				controller: 'ClimateStationsController'
			})
			.state('edit.climateWgn', {
				url: '/climate-wgn',
				templateUrl: 'app/views/edit/climate/wgn.html',
				controller: 'ClimateWgnController'
			});
}]);

var swatControllers = angular.module('swatApp.controllers', []);

swatApp.directive('focusMe', ['$timeout', '$parse', function ($timeout, $parse) {
    return {
        //scope: true,   // optionally create a child scope
        link: function (scope, element, attrs) {
            var model = $parse(attrs.focusMe);
            scope.$watch(model, function (value) {
                if (value === true) {
                    $timeout(function () {
                        element[0].focus();
                    });
                }
            });
            // to address @blesh's comment, set attribute value to 'false'
            // on blur event:
            element.bind('blur', function () {
				if(model.assign!==undefined)
                	scope.$apply(model.assign(scope, false));
            });
        }
    };
}]);
