swatControllers.controller(
	'OutputController',
	['$scope', '$stateParams', 'helpers',
	function ($scope, $stateParams, helpers) {
		$scope.loading = true;
        $scope.helpers = helpers;
        
        $scope.getCurrentProject = function() {
			$scope.current_project = helpers.getSetting('current_project');
		};
		$scope.getCurrentProject();
	}]);