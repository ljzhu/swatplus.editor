swatControllers.controller(
	'SimTimeController',
	['$scope', '$stateParams', 'helpers', 'simulation',
	function ($scope, $stateParams, helpers, simulation) {
		$scope.loading = true;
		$scope.helpers = helpers;

		$scope.sim = {
			jd_start: 1,
			yr_start: 1980,
			jd_end: 1,
			yr_end: 1986,
			step: 0
		};

		$scope.getTimeSim = function() {
			simulation.getTime($scope.current_project.project_db)
				.then(function (response) {
					$scope.sim = response.data;
				})
				.catch(function (error) {
					$scope.error = 'Unable to retrieve time_sim from database: ' + error.Message;
					console.log(error);
				})
				.finally(function () {
					$scope.loading = false;
				});
		};
		$scope.getTimeSim();

		$scope.update = function() {
			$scope.isSaving = true;
			$scope.error = null;

			simulation.updateTime($scope.current_project.project_db, $scope.sim)
				.then(function (response) {
					$scope.saved = true;
				})
				.catch(function (error) {
					$scope.error = 'Unable to save time_sim to database: ' + error.data.message;
					console.log(error);
				})
				.finally(function () {
					$scope.isSaving = false;
				});
		};
		
		$scope.closeSaved = function(index) {
			$scope.saved = false;
		};
	}]);