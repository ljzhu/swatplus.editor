swatControllers.controller(
	'ClimateStationsController',
	['$scope', '$stateParams', 'helpers', 'climate', '$uibModal',
	function ($scope, $stateParams, helpers, climate, $uibModal) {
		$scope.loading = true;
		$scope.helpers = helpers;

		const remote = require('electron').remote;
		const dialog = remote.dialog;

		$scope.sortType = 'name'; // set the default sort type
		$scope.sortReverse = false;  // set the default sort order

		$scope.pagination = {
			currentPage: 1,
			itemsPerPage: 20,
			maxSize: 5,
			total: 0
		};

		$scope.form = {
			weather_data_dir: ''
		};

		$scope.getStations = function() {
			climate.getStationsPage($scope.current_project.project_db, $scope.sortType, $scope.sortReverse, $scope.pagination.currentPage, $scope.pagination.itemsPerPage)
				.then(function (response) {
					$scope.stations = response.data.stations;
					$scope.pagination.total = response.data.total;
					$scope.form.weather_data_dir = response.data.weather_data_dir;
				})
				.catch(function (error) {
					$scope.error = 'Unable to retrieve weather stations from database: ' + error.Message;
					console.log(error);
				})
				.finally(function () {
					$scope.loading = false;
				});
		};
		$scope.getStations();

		$scope.selectWeatherDirectory = function() {
			dialog.showOpenDialog({ 
				properties: ['openDirectory']
			}, function (fileNames) {
				if (fileNames === undefined){
					console.log("No directory selected");
				} else {
					$scope.$apply(function() {
						$scope.form.weather_data_dir = fileNames[0];
					});
				}
			});
		};

		$scope.openCreateModal = function () {
			var modalInstance = $uibModal.open({
				animation: true,
				size: 'lg',
				templateUrl: 'app/views/edit/climate/stations-form.html',
				controller: 'ClimateStationsCreateController',
				resolve: {
					project_db: function () {
						return $scope.current_project.project_db
					}
				}
			});

			modalInstance.result.then(function () { 
				$scope.getStations();
			}, function () { });
		};

		$scope.openUpdateModal = function (id) {
			var modalInstance = $uibModal.open({
				animation: true,
				size: 'lg',
				templateUrl: 'app/views/edit/climate/stations-form.html',
				controller: 'ClimateStationsUpdateController',
				resolve: {
					project_db: function () {
						return $scope.current_project.project_db
					},
					id: function () {
						return id
					}
				}
			});

			modalInstance.result.then(function () { 
				$scope.getStations();
			}, function () { });
		};

		$scope.openDeleteModal = function (id, name) {
			var modalInstance = $uibModal.open({
				animation: true,
				size: 'md',
				templateUrl: 'app/views/edit/delete.html',
				controller: 'ClimateStationsDeleteController',
				resolve: {
					project_db: function () {
						return $scope.current_project.project_db
					},
					name: function () {
						return name
					},
					id: function () {
						return id
					}
				}
			});

			modalInstance.result.then(function () { 
				$scope.getStations();
			}, function () { });
		};

		$scope.openImportModal = function () {
			climate.updateDataDirectory($scope.current_project.project_db, {'weather_data_dir': $scope.form.weather_data_dir})
				.then(function (response) {
					var modalInstance = $uibModal.open({
						animation: true,
						backdrop: 'static',
						keyboard: false,
						size: 'lg',
						templateUrl: 'app/views/edit/climate/stations-import.html',
						controller: 'ClimateStationsImportController',
						resolve: {
							project_db: function () {
								return $scope.current_project.project_db;
							}
						}
					});
		
					modalInstance.result.then(function () { 
						$scope.getStations();
					}, function () { });
				})
				.catch(function (error) {
					$scope.error = 'Unable to update weather data directory in database: ' + error.data.message;
					console.log(error);
				})
				.finally(function () {
					$scope.isSaving = false;
				});						
		};

		$scope.openImportNewFormatModal = function () {
			var modalInstance = $uibModal.open({
				animation: true,
				backdrop: 'static',
				keyboard: false,
				size: 'lg',
				templateUrl: 'app/views/edit/climate/stations-import-new-format.html',
				controller: 'ClimateStationsImportNewFormatController',
				resolve: {
					project_db: function () {
						return $scope.current_project.project_db;
					},
					weather_data_dir: function () {
						return $scope.form.weather_data_dir;
					}
				}
			});

			modalInstance.result.then(function () { 
				$scope.getStations();
			}, function () { });	
		};
	}]);

swatControllers.controller(
	'ClimateStationsCreateController',
	['$scope', '$stateParams', 'helpers', 'climate', '$uibModalInstance', '$q', 'project_db',
	function ($scope, $stateParams, helpers, climate, $uibModalInstance, $q, project_db) {
		$scope.loading = true;
		$scope.helpers = helpers;

		$scope.title = 'Add a weather station';
		$scope.station = {
			wgn_id: null,
			lat: null,
			lon: null
		};

		var sim = 'simulated';
		$scope.stationForm = {
			pcp_sim_obs: sim,
			tmp_sim_obs: sim,
			slr_sim_obs: sim,
			hmd_sim_obs: sim,
			wnd_sim_obs: sim,
			wnd_dir_sim_obs: sim,
			atmo_dep_sim_obs: sim
		};

		$scope.files = function (type, partial_name) {
			var result = [];
			var deferred = $q.defer();

			climate.getFilesAutoComplete(project_db, type, partial_name)
				.then(function (response) {
					angular.forEach(response.data, function (val) {
						result.push(val);
					});
					return deferred.resolve(result);
				})
				.catch(function (error) {
					console.log(error);
				});
			return deferred.promise;
		};

		$scope.wgns = function (partial_name) {
			var result = [];
			var deferred = $q.defer();

			climate.getWgnAutoComplete(project_db, partial_name)
				.then(function (response) {
					angular.forEach(response.data, function (val) {
						result.push(val);
					});
					return deferred.resolve(result);
				})
				.catch(function (error) {
					console.log(error);
				});
			return deferred.promise;
		};

		$scope.save = function() {
			$scope.isSaving = true;
			$scope.error = null;

			var sim_db = 'sim';
			if ($scope.stationForm.pcp_sim_obs == sim) $scope.station.pcp = sim_db;
			if ($scope.stationForm.tmp_sim_obs == sim) $scope.station.tmp = sim_db;
			if ($scope.stationForm.slr_sim_obs == sim) $scope.station.slr = sim_db;
			if ($scope.stationForm.hmd_sim_obs == sim) $scope.station.hmd = sim_db;
			if ($scope.stationForm.wnd_sim_obs == sim) $scope.station.wnd = sim_db;
			if ($scope.stationForm.wnd_dir_sim_obs == sim) $scope.station.wnd_dir = sim_db;
			if ($scope.stationForm.atmo_dep_sim_obs == sim) $scope.station.atmo_dep = sim_db;

			$scope.station.name = $scope.station.name.replace(/ /g,"_");

			climate.createStation(project_db, $scope.station)
				.then(function (response) {
					$uibModalInstance.close();
				})
				.catch(function (error) {
					$scope.error = 'Unable to save weather station to database: ' + error.data.message;
					console.log(error);
				})
				.finally(function () {
					$scope.isSaving = false;
				});
		};

		$scope.cancel = function () {
			$uibModalInstance.dismiss('cancel');
		};
	}]);

swatControllers.controller(
	'ClimateStationsUpdateController',
	['$scope', '$stateParams', 'helpers', 'climate', '$uibModalInstance', '$q', 'project_db', 'id',
	function ($scope, $stateParams, helpers, climate, $uibModalInstance, $q, project_db, id) {
		$scope.loading = true;
		$scope.helpers = helpers;

		$scope.title = 'Update weather station';
		$scope.station = {
			wgn_id: null,
			lat: null,
			lon: null
		};

		var sim_db = 'sim';
		var sim = 'simulated';
		var obs = 'observed';
		
		$scope.getStation = function() {
			climate.getStation(project_db, id)
				.then(function (response) {
					$scope.station = response.data;
					$scope.stationForm = {
						pcp_sim_obs: $scope.station.pcp == sim_db ? sim : obs,
						tmp_sim_obs: $scope.station.tmp == sim_db ? sim : obs,
						slr_sim_obs: $scope.station.slr == sim_db ? sim : obs,
						hmd_sim_obs: $scope.station.hmd == sim_db ? sim : obs,
						wnd_sim_obs: $scope.station.wnd == sim_db ? sim : obs,
						wnd_dir_sim_obs: $scope.station.wnd_dir == sim_db ? sim : obs,
						atmo_dep_sim_obs: $scope.station.atmo_dep == sim_db ? sim : obs
					};
				})
				.catch(function (error) {
					$scope.error = 'Unable to retrieve weather station from database: ' + error.Message;
					console.log(error);
				})
				.finally(function () {
					$scope.loading = false;
				});
		};
		$scope.getStation();	
		
		$scope.files = function (type, partial_name) {
			var result = [];
			var deferred = $q.defer();

			climate.getFilesAutoComplete(project_db, type, partial_name)
				.then(function (response) {
					angular.forEach(response.data, function (val) {
						result.push(val);
					});
					return deferred.resolve(result);
				})
				.catch(function (error) {
					console.log(error);
				});
			return deferred.promise;
		};
		
		$scope.wgns = function (partial_name) {
			var result = [];
			var deferred = $q.defer();

			climate.getWgnAutoComplete(project_db, partial_name)
				.then(function (response) {
					angular.forEach(response.data, function (val) {
						result.push(val);
					});
					return deferred.resolve(result);
				})
				.catch(function (error) {
					console.log(error);
				});
			return deferred.promise;
		};

		$scope.save = function() {
			$scope.isSaving = true;
			$scope.error = null;

			if ($scope.stationForm.pcp_sim_obs == sim) $scope.station.pcp = sim_db;
			if ($scope.stationForm.tmp_sim_obs == sim) $scope.station.tmp = sim_db;
			if ($scope.stationForm.slr_sim_obs == sim) $scope.station.slr = sim_db;
			if ($scope.stationForm.hmd_sim_obs == sim) $scope.station.hmd = sim_db;
			if ($scope.stationForm.wnd_sim_obs == sim) $scope.station.wnd = sim_db;
			if ($scope.stationForm.wnd_dir_sim_obs == sim) $scope.station.wnd_dir = sim_db;
			if ($scope.stationForm.atmo_dep_sim_obs == sim) $scope.station.atmo_dep = sim_db;

			$scope.station.name = $scope.station.name.replace(/ /g,"_");

			climate.updateStation(project_db, id, $scope.station)
				.then(function (response) {
					$uibModalInstance.close();
				})
				.catch(function (error) {
					$scope.error = 'Unable to save weather station to database: ' + error.data.message;
					console.log(error);
				})
				.finally(function () {
					$scope.isSaving = false;
				});
		};

		$scope.cancel = function () {
			$uibModalInstance.dismiss('cancel');
		};
	}]);

swatControllers.controller(
	'ClimateStationsDeleteController',
	['$scope', '$stateParams', 'helpers', 'climate', '$uibModalInstance', 'project_db', 'name', 'id',
	function ($scope, $stateParams, helpers, climate, $uibModalInstance, project_db, name, id) {
		$scope.loading = true;
		$scope.helpers = helpers;

		$scope.name = name;

		$scope.ok = function() {
			$scope.isSaving = true;
			$scope.error = null;

			climate.deleteStation(project_db, id)
				.then(function (response) {
					$uibModalInstance.close();
				})
				.catch(function (error) {
					$scope.error = 'Unable to delete weather station from database: ' + error.data.message;
					console.log(error);
				})
				.finally(function () {
					$scope.isSaving = false;
				});
		};

		$scope.cancel = function () {
			$uibModalInstance.dismiss('cancel');
		};
	}]);

swatControllers.controller(
	'ClimateStationsImportController',
	['$scope', '$stateParams', 'helpers', 'climate', '$uibModalInstance', 'project_db', 
	function ($scope, $stateParams, helpers, climate, $uibModalInstance, project_db) {
		$scope.loading = true;
		$scope.helpers = helpers;

		$scope.options = {
			delete_existing: false
		};

		$scope.progress = {
			percent: 0,
			message: ''
		}

		$scope.ok = function() {
			$scope.isSaving = true;
			$scope.error = null;

			var del_ex = $scope.options.delete_existing ? 'y' : 'n';

			$scope.process = helpers.getApiProc('swatplus_weatherimport', [project_db, del_ex])
			$scope.process.stdout.on('data', (data) => {
				$scope.$apply(function() {
					$scope.progress = helpers.getApiOutput(data);
				});
			});
			
			$scope.process.stderr.on('data', (data) => {
				$scope.error = "There was an error processing your data."
				console.log(`stderr: ${data}`);
			});
			
			$scope.process.on('close', (code) => {
				$uibModalInstance.close();
			});
		};

		$scope.cancel = function () {
			if ($scope.process !== undefined)
				$scope.process.kill();
			$uibModalInstance.close();
		};
	}]);

swatControllers.controller(
	'ClimateStationsImportNewFormatController',
	['$scope', '$stateParams', 'helpers', 'climate', '$uibModalInstance', 'project_db', 'weather_data_dir',
	function ($scope, $stateParams, helpers, climate, $uibModalInstance, project_db, weather_data_dir) {
		$scope.loading = true;
		$scope.helpers = helpers;

		const remote = require('electron').remote;
		const dialog = remote.dialog;

		$scope.options = {
			format: 'SWAT2012',
			delete_existing: false,
			source_dir: '',
			save_dir: weather_data_dir
		};

		$scope.progress = {
			percent: 0,
			message: ''
		}

		$scope.selectSourceDirectory = function() {
			dialog.showOpenDialog({ 
				properties: ['openDirectory']
			}, function (fileNames) {
				if (fileNames === undefined){
					console.log("No directory selected");
				} else {
					$scope.$apply(function() {
						$scope.options.source_dir = fileNames[0];
					});
				}
			});
		};

		$scope.selectSaveDirectory = function() {
			dialog.showOpenDialog({ 
				properties: ['openDirectory']
			}, function (fileNames) {
				if (fileNames === undefined){
					console.log("No directory selected");
				} else {
					$scope.$apply(function() {
						$scope.options.save_dir = fileNames[0];
					});
				}
			});
		};

		$scope.ok = function() {
			$scope.isSaving = true;
			$scope.error = null;

			var del_ex = $scope.options.delete_existing ? 'y' : 'n';

			climate.updateDataDirectory(project_db, {'weather_data_dir': $scope.options.save_dir})
				.then(function (response) {
					var script_name = '';
					if ($scope.options.format == 'SWAT2012')
						script_name = 'swatplus_weatherimport_swat2012';

					if (script_name != '') {
						$scope.process = helpers.getApiProc(script_name, [$scope.options.source_dir, project_db, del_ex])
						$scope.process.stdout.on('data', (data) => {
							$scope.$apply(function() {
								$scope.progress = helpers.getApiOutput(data);
							});
						});
						
						$scope.process.stderr.on('data', (data) => {
							$scope.error = "There was an error processing your data."
							console.log(`stderr: ${data}`);
						});
						
						$scope.process.on('close', (code) => {
							$uibModalInstance.close();
						});
					}					
				})
				.catch(function (error) {
					$scope.error = 'Unable to update weather data directory in database: ' + error.data.message;
					console.log(error);
				});
		};

		$scope.cancel = function () {
			if ($scope.process !== undefined)
				$scope.process.kill();
			$uibModalInstance.dismiss('cancel');
		};
	}]);

swatControllers.controller(
	'ClimateWgnController',
	['$scope', '$stateParams', 'helpers', 'climate', '$uibModal',
	function ($scope, $stateParams, helpers, climate, $uibModal) {
		$scope.loading = true;
		$scope.helpers = helpers;

		const remote = require('electron').remote;
		const dialog = remote.dialog;

		$scope.sortType = 'name'; // set the default sort type
		$scope.sortReverse = false;  // set the default sort order

		$scope.pagination = {
			currentPage: 1,
			itemsPerPage: 20,
			maxSize: 5,
			total: 0
		};

		$scope.form = {
			wgn_db: '',
			wgn_table_name: ''
		};

		$scope.getWgns = function() {
			climate.getWgnsPage($scope.current_project.project_db, $scope.sortType, $scope.sortReverse, $scope.pagination.currentPage, $scope.pagination.itemsPerPage)
				.then(function (response) {
					$scope.wgns = response.data.wgns;
					$scope.pagination.total = response.data.total;
					$scope.form.wgn_db = response.data.wgn_db;
					$scope.form.wgn_table_name = response.data.wgn_table_name;
				})
				.catch(function (error) {
					$scope.error = 'Unable to retrieve weather generators from database: ' + error.Message;
					console.log(error);
				})
				.finally(function () {
					$scope.loading = false;
				});
		};
		$scope.getWgns();

		$scope.selectWgnDb = function() {
			dialog.showOpenDialog({ 
				filters: [{ name: 'SQLite', extensions: ['sqlite','db'] },{ name: 'All files', extensions: ['*'] }]
			}, function (fileNames) {
				if (fileNames === undefined){
					console.log("No directory selected");
				} else {
					$scope.$apply(function() {
						$scope.form.wgn_db = fileNames[0];
					});
				}
			});
		};

		$scope.openCreateModal = function () {
			var modalInstance = $uibModal.open({
				animation: true,
				size: 'lg',
				templateUrl: 'app/views/edit/climate/wgn-form.html',
				controller: 'ClimateWgnCreateController',
				resolve: {
					project_db: function () {
						return $scope.current_project.project_db
					}
				}
			});

			modalInstance.result.then(function () { 
				$scope.getWgns();
			}, function () { });
		};

		$scope.openUpdateModal = function (id) {
			var modalInstance = $uibModal.open({
				animation: true,
				size: 'lg',
				templateUrl: 'app/views/edit/climate/wgn-form.html',
				controller: 'ClimateWgnUpdateController',
				resolve: {
					project_db: function () {
						return $scope.current_project.project_db
					},
					id: function () {
						return id
					}
				}
			});

			modalInstance.result.then(function () { 
				$scope.getWgns();
			}, function () { });
		};

		$scope.openDeleteModal = function (id, name) {
			var modalInstance = $uibModal.open({
				animation: true,
				size: 'md',
				templateUrl: 'app/views/edit/delete.html',
				controller: 'ClimateWgnDeleteController',
				resolve: {
					project_db: function () {
						return $scope.current_project.project_db
					},
					name: function () {
						return name
					},
					id: function () {
						return id
					}
				}
			});

			modalInstance.result.then(function () { 
				$scope.getWgns();
			}, function () { });
		};

		$scope.openImportModal = function () {
			climate.updateWgnDb($scope.current_project.project_db, {'wgn_db': $scope.form.wgn_db, 'wgn_table_name': $scope.form.wgn_table_name})
				.then(function (response) {
					var modalInstance = $uibModal.open({
						animation: true,
						backdrop: 'static',
						keyboard: false,
						size: 'lg',
						templateUrl: 'app/views/edit/climate/wgn-import.html',
						controller: 'ClimateWgnImportController',
						resolve: {
							project_db: function () {
								return $scope.current_project.project_db;
							}
						}
					});
		
					modalInstance.result.then(function () { 
						$scope.getWgns();
					}, function () { });
				})
				.catch(function (error) {
					$scope.error = 'Unable to update weather generator in database: ' + error.data.message;
					console.log(error);
				})
				.finally(function () {
					$scope.isSaving = false;
				});						
		};

		$scope.openValuesCreateModal = function (wgn_id, wgn_name) {
			var modalInstance = $uibModal.open({
				animation: true,
				size: 'lg',
				templateUrl: 'app/views/edit/climate/wgn-values-form.html',
				controller: 'ClimateWgnValuesCreateController',
				resolve: {
					project_db: function () {
						return $scope.current_project.project_db
					},
					wgn_id: function () {
						return wgn_id
					},
					wgn_name: function () {
						return wgn_name
					}
				}
			});

			modalInstance.result.then(function () { 
				$scope.getWgns();
			}, function () { });
		};

		$scope.openValuesUpdateModal = function (id, wgn_name) {
			var modalInstance = $uibModal.open({
				animation: true,
				size: 'lg',
				templateUrl: 'app/views/edit/climate/wgn-values-form.html',
				controller: 'ClimateWgnValuesUpdateController',
				resolve: {
					project_db: function () {
						return $scope.current_project.project_db
					},
					id: function () {
						return id
					},
					wgn_name: function () {
						return wgn_name
					}
				}
			});

			modalInstance.result.then(function () { 
				$scope.getWgns();
			}, function () { });
		};

		$scope.openValuesDeleteModal = function (id, name) {
			var modalInstance = $uibModal.open({
				animation: true,
				size: 'md',
				templateUrl: 'app/views/edit/delete.html',
				controller: 'ClimateWgnValuesDeleteController',
				resolve: {
					project_db: function () {
						return $scope.current_project.project_db
					},
					name: function () {
						return name
					},
					id: function () {
						return id
					}
				}
			});

			modalInstance.result.then(function () { 
				$scope.getWgns();
			}, function () { });
		};
	}]);

swatControllers.controller(
	'ClimateWgnCreateController',
	['$scope', '$stateParams', 'helpers', 'climate', '$uibModalInstance', '$q', 'project_db',
	function ($scope, $stateParams, helpers, climate, $uibModalInstance, $q, project_db) {
		$scope.loading = true;
		$scope.helpers = helpers;

		$scope.title = 'Add a weather generator';

		$scope.save = function() {
			$scope.isSaving = true;
			$scope.error = null;

			$scope.wgn.name = $scope.wgn.name.replace(/ /g,"_");

			climate.createWgn(project_db, $scope.wgn)
				.then(function (response) {
					$uibModalInstance.close();
				})
				.catch(function (error) {
					$scope.error = 'Unable to save weather generator to database: ' + error.data.message;
					console.log(error);
				})
				.finally(function () {
					$scope.isSaving = false;
				});
		};

		$scope.cancel = function () {
			$uibModalInstance.dismiss('cancel');
		};
	}]);

swatControllers.controller(
	'ClimateWgnUpdateController',
	['$scope', '$stateParams', 'helpers', 'climate', '$uibModalInstance', '$q', 'project_db', 'id',
	function ($scope, $stateParams, helpers, climate, $uibModalInstance, $q, project_db, id) {
		$scope.loading = true;
		$scope.helpers = helpers;

		$scope.title = 'Update weather generator';
		
		$scope.getWgn = function() {
			climate.getWgn(project_db, id)
				.then(function (response) {
					$scope.wgn = response.data;
				})
				.catch(function (error) {
					$scope.error = 'Unable to retrieve weather generator from database: ' + error.Message;
					console.log(error);
				})
				.finally(function () {
					$scope.loading = false;
				});
		};
		$scope.getWgn();

		$scope.save = function() {
			$scope.isSaving = true;
			$scope.error = null;

			$scope.wgn.name = $scope.wgn.name.replace(/ /g,"_");

			climate.updateWgn(project_db, id, $scope.wgn)
				.then(function (response) {
					$uibModalInstance.close();
				})
				.catch(function (error) {
					$scope.error = 'Unable to save weather generator to database: ' + error.data.message;
					console.log(error);
				})
				.finally(function () {
					$scope.isSaving = false;
				});
		};

		$scope.cancel = function () {
			$uibModalInstance.dismiss('cancel');
		};
	}]);

swatControllers.controller(
	'ClimateWgnDeleteController',
	['$scope', '$stateParams', 'helpers', 'climate', '$uibModalInstance', 'project_db', 'name', 'id',
	function ($scope, $stateParams, helpers, climate, $uibModalInstance, project_db, name, id) {
		$scope.loading = true;
		$scope.helpers = helpers;

		$scope.name = name;

		$scope.ok = function() {
			$scope.isSaving = true;
			$scope.error = null;

			climate.deleteWgn(project_db, id)
				.then(function (response) {
					$uibModalInstance.close();
				})
				.catch(function (error) {
					$scope.error = 'Unable to delete weather generator from database: ' + error.data.message;
					console.log(error);
				})
				.finally(function () {
					$scope.isSaving = false;
				});
		};

		$scope.cancel = function () {
			$uibModalInstance.dismiss('cancel');
		};
	}]);

swatControllers.controller(
	'ClimateWgnImportController',
	['$scope', '$stateParams', 'helpers', 'climate', '$uibModalInstance', 'project_db', 
	function ($scope, $stateParams, helpers, climate, $uibModalInstance, project_db) {
		$scope.loading = true;
		$scope.helpers = helpers;

		$scope.options = {
			delete_existing: false
		};

		$scope.progress = {
			percent: 0,
			message: ''
		}

		$scope.ok = function() {
			$scope.isSaving = true;
			$scope.error = null;

			var del_ex = $scope.options.delete_existing ? 'y' : 'n';

			$scope.process = helpers.getApiProc('swatplus_wgnimport', [project_db, del_ex])
			$scope.process.stdout.on('data', (data) => {
				$scope.$apply(function() {
					$scope.progress = helpers.getApiOutput(data);
				});
			});
			
			$scope.process.stderr.on('data', (data) => {
				$scope.error = "There was an error processing your data."
				console.log(`stderr: ${data}`);
			});
			
			$scope.process.on('close', (code) => {
				$uibModalInstance.close();
			});
		};

		$scope.cancel = function () {
			if ($scope.process !== undefined)
				$scope.process.kill();
			$uibModalInstance.close();
		};
	}]);

swatControllers.controller(
	'ClimateWgnValuesCreateController',
	['$scope', '$stateParams', 'helpers', 'climate', '$uibModalInstance', '$q', 'project_db', 'wgn_id', 'wgn_name',
	function ($scope, $stateParams, helpers, climate, $uibModalInstance, $q, project_db, wgn_id, wgn_name) {
		$scope.loading = true;
		$scope.helpers = helpers;

		$scope.title = 'Add values to ' + wgn_name;
		$scope.wgn = {
			weather_wgn_cli_id: wgn_id,
			month: 1
		};

		$scope.save = function() {
			$scope.isSaving = true;
			$scope.error = null;

			climate.createWgnMonth(project_db, wgn_id, $scope.wgn)
				.then(function (response) {
					$uibModalInstance.close();
				})
				.catch(function (error) {
					$scope.error = 'Unable to save weather generator to database: ' + error.data.message;
					console.log(error);
				})
				.finally(function () {
					$scope.isSaving = false;
				});
		};

		$scope.cancel = function () {
			$uibModalInstance.dismiss('cancel');
		};
	}]);

swatControllers.controller(
	'ClimateWgnValuesUpdateController',
	['$scope', '$stateParams', 'helpers', 'climate', '$uibModalInstance', '$q', 'project_db', 'id', 'wgn_name',
	function ($scope, $stateParams, helpers, climate, $uibModalInstance, $q, project_db, id, wgn_name) {
		$scope.loading = true;
		$scope.helpers = helpers;

		$scope.title = 'Update ' + wgn_name + ' values';
		
		$scope.getWgn = function() {
			climate.getWgnMonth(project_db, id)
				.then(function (response) {
					$scope.wgn = response.data;
				})
				.catch(function (error) {
					$scope.error = 'Unable to retrieve weather generator values from database: ' + error.Message;
					console.log(error);
				})
				.finally(function () {
					$scope.loading = false;
				});
		};
		$scope.getWgn();

		$scope.save = function() {
			$scope.isSaving = true;
			$scope.error = null;

			climate.updateWgnMonth(project_db, id, $scope.wgn)
				.then(function (response) {
					$uibModalInstance.close();
				})
				.catch(function (error) {
					$scope.error = 'Unable to save weather generator values to database: ' + error.data.message;
					console.log(error);
				})
				.finally(function () {
					$scope.isSaving = false;
				});
		};

		$scope.cancel = function () {
			$uibModalInstance.dismiss('cancel');
		};
	}]);

swatControllers.controller(
	'ClimateWgnValuesDeleteController',
	['$scope', '$stateParams', 'helpers', 'climate', '$uibModalInstance', 'project_db', 'name', 'id',
	function ($scope, $stateParams, helpers, climate, $uibModalInstance, project_db, name, id) {
		$scope.loading = true;
		$scope.helpers = helpers;

		$scope.name = name;

		$scope.ok = function() {
			$scope.isSaving = true;
			$scope.error = null;

			climate.deleteWgnMonth(project_db, id)
				.then(function (response) {
					$uibModalInstance.close();
				})
				.catch(function (error) {
					$scope.error = 'Unable to delete weather generator values from database: ' + error.data.message;
					console.log(error);
				})
				.finally(function () {
					$scope.isSaving = false;
				});
		};

		$scope.cancel = function () {
			$uibModalInstance.dismiss('cancel');
		};
	}]);