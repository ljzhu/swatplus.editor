swatControllers.controller(
	'ProjectSetupController',
	['$scope', '$stateParams', 'helpers', 'projectSetup',
	function ($scope, $stateParams, helpers, projectSetup) {
		$scope.loading = true;
		$scope.helpers = helpers;

		var version = '1.0.0.dev1';

		const remote = require('electron').remote;
		const dialog = remote.dialog;
		const fs = require('fs');
		const path = require('path');

		$scope.new_project = {};
		$scope.existing_project = {};

		$scope.getSettingsFile = function() {
			dialog.showOpenDialog({ 
				filters: [{ name: 'Json', extensions: ['json'] }]
			}, function (fileNames) {
				if (fileNames === undefined){
					console.log("No file selected");
				} else {
					$scope.existing_project.settings = fileNames[0];
					$scope.$apply();
				}
			});
		};

		$scope.getReferenceDb = function() {
			dialog.showOpenDialog({ 
				filters: [{ name: 'SQLite', extensions: ['sqlite','db'] },{ name: 'All files', extensions: ['*'] }]
			}, function (fileNames) {
				if (fileNames === undefined){
					console.log("No file selected");
				} else {
					$scope.new_project.reference_db = fileNames[0];
					$scope.$apply();
				}
			});
		};

		$scope.getProjectDb = function() {
			dialog.showOpenDialog({ 
				filters: [{ name: 'SQLite', extensions: ['sqlite','db'] },{ name: 'All files', extensions: ['*'] }]
			}, function (fileNames) {
				if (fileNames === undefined){
					console.log("No file selected");
				} else {
					$scope.new_project.project_db = fileNames[0];
					$scope.$apply();
				}
			});
		};

		$scope.getCurrentReferenceDb = function() {
			dialog.showOpenDialog({ 
				filters: [{ name: 'SQLite', extensions: ['sqlite','db'] },{ name: 'All files', extensions: ['*'] }]
			}, function (fileNames) {
				if (fileNames === undefined){
					console.log("No file selected");
				} else {
					$scope.current_project.reference_db = fileNames[0];
					$scope.$apply();
				}
			});
		};

		$scope.getCurrentProjectDb = function() {
			dialog.showOpenDialog({ 
				filters: [{ name: 'SQLite', extensions: ['sqlite','db'] },{ name: 'All files', extensions: ['*'] }]
			}, function (fileNames) {
				if (fileNames === undefined){
					console.log("No file selected");
				} else {
					$scope.current_project.project_db = fileNames[0];
					$scope.$apply();
				}
			});
		};

		$scope.loadRecentProject = function(settings) {
			$scope.existing_project.settings = settings;
			$scope.loadProject();
		};

		$scope.loadProject = function() {
			$scope.isSaving = true;

			fs.readFile($scope.existing_project.settings, 'utf8', function(err, data) {
				if (err)
					console.log(err);
				else {
					var settings_data = JSON.parse(data);
					var p = settings_data['swatplus-project'];

					var rel_dir = path.dirname($scope.existing_project.settings);

					helpers.addSetting('current_project.project_name', p.name);
					helpers.addSetting('current_project.settings', $scope.existing_project.settings);
					helpers.addSetting('current_project.project_db', path.resolve(rel_dir, p.databases.project));
					helpers.addSetting('current_project.reference_db', path.resolve(rel_dir, p.databases.datasets));

					$scope.getCurrentProject();
					$scope.isLoadOpen = false;
					$scope.isSaving = false;
					$scope.$apply();
				}
			});
		};

		$scope.createProject = function() {
			$scope.isSaving = true;
			$scope.new_project.editor_version = version;

			var sanitized_name = $scope.new_project.project_name.replace(/[^a-z0-9]/gi, '_').toLowerCase();
			var default_name = sanitized_name + '.json';			

			dialog.showSaveDialog({ 
				filters: [{ name: 'Json', extensions: ['json'] }],
				defaultPath: '*/' + default_name
			}, function (file_name) {
				if (file_name === undefined) return;

				var save_dir = path.dirname(file_name);

				if ($scope.new_project.project_db === undefined) {
					var db_name = sanitized_name + '.sqlite';
					$scope.new_project.project_db = path.join(save_dir, db_name);
				}

				var rel_project_db = path.relative(save_dir, $scope.new_project.project_db);
				var rel_datasets_db = path.relative(save_dir, $scope.new_project.reference_db);

				var settings_file_data = {
					'swatplus-project': {
						'version': $scope.new_project.editor_version,
						'name': $scope.new_project.project_name,
						'databases': {
							'project': rel_project_db,
							'datasets': rel_datasets_db
						},
						'model': 'SWAT+'
					}
				};

				fs.writeFile(file_name, JSON.stringify(settings_file_data, null, '\t'), function(err) {
					projectSetup.create($scope.new_project)
						.then(function (response) {
							helpers.addSetting('current_project.project_name', $scope.new_project.project_name);
							helpers.addSetting('current_project.settings', file_name);
							helpers.addSetting('current_project.project_db', $scope.new_project.project_db);
							helpers.addSetting('current_project.reference_db', $scope.new_project.reference_db);

							$scope.getCurrentProject();
							$scope.isCreateOpen = false;
						})
						.catch(function (error) {
							$scope.error = 'Unable to setup project database: ' + error.Message;
							console.log(error);
						})
						.finally(function () {
							$scope.isSaving = false;
						});
				});
			});
		};

		$scope.updateProject = function() {
			$scope.isSaving = true;
			$scope.current_project.editor_version = version;

			var save_dir = path.dirname($scope.current_project.settings);
			var rel_project_db = path.relative(save_dir, $scope.current_project.project_db);
			var rel_datasets_db = path.relative(save_dir, $scope.current_project.reference_db);

			var settings_file_data = {
				'swatplus-project': {
					'version': $scope.current_project.editor_version,
					'name': $scope.current_project.project_name,
					'databases': {
						'project': rel_project_db,
						'datasets': rel_datasets_db
					},
					'model': 'SWAT+'
				}
			};

			fs.writeFile($scope.current_project.settings, JSON.stringify(settings_file_data, null, '\t'), function(err) {
				projectSetup.create($scope.current_project)
					.then(function (response) {
						helpers.addSetting('current_project.project_name', $scope.current_project.project_name);
						helpers.addSetting('current_project.settings', $scope.current_project.settings);
						helpers.addSetting('current_project.project_db', $scope.current_project.project_db);
						helpers.addSetting('current_project.reference_db', $scope.current_project.reference_db);
						$scope.getCurrentProject();
					})
					.catch(function (error) {
						$scope.error = 'Unable to update project database: ' + error.Message;
						console.log(error);
					})
					.finally(function () {
						$scope.isSaving = false;
						//$scope.$apply();
					});
			});
		};
		
		$scope.getRecentProjects = function() {
			$scope.recent_projects = helpers.getSetting('recent_projects');
		};
		$scope.getRecentProjects();

		$scope.getCurrentProject = function() {
			$scope.current_project = helpers.getSetting('current_project');
			if ($scope.current_project !== undefined)
				helpers.pushRecentProject($scope.current_project.project_name, $scope.current_project.settings);
			$scope.getRecentProjects();
		};
		$scope.getCurrentProject();
	}]);