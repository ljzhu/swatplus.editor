swatControllers.controller(
	'WriteInputsController',
	['$scope', '$stateParams', 'helpers', 'projectSetup', '$uibModal',
	function ($scope, $stateParams, helpers, projectSetup, $uibModal) {
		$scope.loading = true;
		$scope.helpers = helpers;
		
		const remote = require('electron').remote;
		const dialog = remote.dialog;
		const path = require('path');
        
        $scope.getCurrentProject = function() {
			$scope.current_project = helpers.getSetting('current_project');
		};
		$scope.getCurrentProject();

		$scope.getConfig = function() {
			projectSetup.getInputFilesSettings($scope.current_project.project_db)
				.then(function (response) {
					$scope.config = response.data;

					if ($scope.config.input_files_dir == null)
						$scope.config.input_files_dir = path.dirname($scope.current_project.settings);
				})
				.catch(function (error) {
					$scope.error = 'Unable to retrieve weather stations from database: ' + error.Message;
					console.log(error);
				})
				.finally(function () {
					$scope.loading = false;
				});
		};
		$scope.getConfig();

		$scope.selectDirectory = function() {
			dialog.showOpenDialog({ 
				properties: ['openDirectory']
			}, function (fileNames) {
				if (fileNames !== undefined){
					$scope.$apply(function() {
						$scope.config.input_files_dir = fileNames[0];
					});
				}
			});
		};

		$scope.save = function () {
			projectSetup.updateInputFilesSettings($scope.current_project.project_db, {'input_files_dir': $scope.config.input_files_dir})
				.then(function (response) {
					var modalInstance = $uibModal.open({
						animation: true,
						backdrop: 'static',
						keyboard: false,
						size: 'lg',
						templateUrl: 'app/views/write-inputs-modal.html',
						controller: 'WriteImputsConfirmController',
						resolve: {
							project_db: function () {
								return $scope.current_project.project_db;
							}
						}
					});
		
					modalInstance.result.then(function () { 
						$scope.getConfig();
					}, function () { });
				})
				.catch(function (error) {
					$scope.error = 'Unable to update input files directory in database: ' + error.data.message;
					console.log(error);
				})
				.finally(function () {
					$scope.isSaving = false;
				});						
		};
	}]);

swatControllers.controller(
	'WriteImputsConfirmController',
	['$scope', '$stateParams', 'helpers', '$uibModalInstance', 'project_db', 
	function ($scope, $stateParams, helpers, $uibModalInstance, project_db) {
		$scope.loading = true;
		$scope.helpers = helpers;

		$scope.progress = {
			percent: 0,
			message: ''
		}

		$scope.ok = function() {
			$scope.isSaving = true;
			$scope.error = null;

			$scope.process = helpers.getApiProc('swatplus_write_files', [project_db])
			$scope.process.stdout.on('data', (data) => {
				$scope.$apply(function() {
					$scope.progress = helpers.getApiOutput(data);
				});
			});
			
			$scope.process.stderr.on('data', (data) => {
				$scope.error = "There was an error processing your data."
				console.log(`stderr: ${data}`);
			});
			
			$scope.process.on('close', (code) => {
				$uibModalInstance.close();
			});
		};

		$scope.cancel = function () {
			if ($scope.process !== undefined)
				$scope.process.kill();
			$uibModalInstance.close();
		};
	}]);