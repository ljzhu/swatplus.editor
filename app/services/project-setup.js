angular.module('swatApp.projectSetup.service', [])
	.factory('projectSetup', ['$http', function ($http) {
		var urlBase = 'http://localhost:5000/setup';

		return {
			create: function (project) {
				return $http({
					method: 'POST',
					url: urlBase,
					data: project
				});
			},

			test: function () {
				return $http({
					method: 'GET',
					url: 'http://localhost:5000'
				});
			},

			getInputFilesSettings: function (project_db) {
				return $http({
					method: 'GET',
					url: urlBase + '/inputfiles/' + encodeURIComponent(project_db)
				});
			},

			updateInputFilesSettings: function (project_db, data) {
				return $http({
					method: 'PUT',
					url: urlBase + '/inputfiles/' + encodeURIComponent(project_db),
					data: data
				});
			},
		};

	}]);