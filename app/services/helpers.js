angular.module('swatApp.helpers', [])
	.factory('helpers', ['$http', '$filter', function ($http, $filter) {
		const DEV_MODE = true;

		const Store = require('electron-store');
		const store = new Store();
		const path = require('path');

		return {
			getApiPath: function(script_name) {
				if (DEV_MODE) {
					return path.join(__dirname, 'api', script_name + '.py');
				} else {
					return path.join(__dirname, 'api', 'dist', script_name, script_name + '.exe');
				}
			},
			getApiProc: function(script_name, args) {
				if (DEV_MODE) {
					var script = path.join(__dirname, 'api', script_name + '.py');
					args.unshift(script);
					return require('child_process').spawn('python', args);
				} else {
					var script = path.join(__dirname, 'api', 'dist', script_name, script_name);
					return require('child_process').spawn(script, args);
				}
			},
			getApiOutput: function(data) {
				try {
					var san = data.toString().split(/\r?\n/)
					if (san.length > 0) {
						if (san[san.length - 1] == '')
							san.pop(); 
						if (san.length > 0 && san[san.length - 1] != '')
							return JSON.parse(san[san.length - 1]);
					}
				} catch(err) {
					return null;
				}
			},
			openInBrowser: function (url) {
				require('electron').shell.openExternal(url);
			},
			openFileDialog: function (elementToSet, filters) {
				const remote = require('electron').remote;
				const dialog = remote.dialog;

				dialog.showOpenDialog({ filters: filters}, function (fileNames) {
						if (fileNames === undefined){
							console.log("No file selected");
						} else {
							return fileNames[0];
						}
					});
			},
			addSetting: function(key, value) {
				store.set(key, value);
			},
			getSetting: function(key) {
				return store.get(key);
			},
			deleteSetting: function(key) {
				store.delete(key);
			},
			pushRecentProject: function(name, settings) {
				var project = {
					'project_name': name,
					'settings': settings
				};

				var recent_projects = store.get('recent_projects');
				if (recent_projects === undefined)
					recent_projects = [];

				var exists = $filter('filter')(recent_projects, { settings: settings })[0];
				if (exists) {
					exists.project_name = name;
					store.set('recent_projects', recent_projects);
				}

				if (exists == null) {
					recent_projects.unshift(project);

					if (recent_projects.length > 4)
						recent_projects.pop();

					store.set('recent_projects', recent_projects);
				}
			}
		};

	}]);