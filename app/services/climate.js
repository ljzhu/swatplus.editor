angular.module('swatApp.climate.service', [])
.factory('climate', ['$http', function ($http) {
    var urlBase = 'http://localhost:5000/climate/';

    return {
        getStations: function (project_db) {
			return $http({
				method: 'GET',
				url: urlBase + 'stations/' + encodeURIComponent(project_db)
			});
		},

		getStationsPage: function (project_db, sortType, sortReverse, page, itemsPerPage) {
			return $http({
				method: 'GET',
				url: urlBase + 'stations/' + encodeURIComponent(project_db) + '/' + sortType + '/' + sortReverse + '/' + page + '/' + itemsPerPage
			});
		},

        getStation: function (project_db, id) {
            return $http({
                method: 'GET',
                url: urlBase + 'stations/' + encodeURIComponent(project_db) + '/' + id
            });
		},
		
		createStation: function (project_db, data) {
            return $http({
                method: 'POST',
                url: urlBase + 'stations/' + encodeURIComponent(project_db),
                data: data
            });
        },
		
		updateStation: function (project_db, id, data) {
            return $http({
                method: 'PUT',
                url: urlBase + 'stations/' + encodeURIComponent(project_db) + '/' + id,
                data: data
            });
		},
		
		deleteStation: function (project_db, id) {
            return $http({
                method: 'DELETE',
                url: urlBase + 'stations/' + encodeURIComponent(project_db) + '/' + id
            });
        },
		
		updateDataDirectory: function (project_db, data) {
            return $http({
                method: 'PUT',
                url: urlBase + 'stations/directory/' + encodeURIComponent(project_db),
                data: data
            });
        },
        
        getFilesAutoComplete: function (project_db, type, partial_name) {
			return $http({
				method: 'GET',
				url: urlBase + 'stations/files/' + encodeURIComponent(project_db) + '/' + type + '/' + partial_name
			});
        },

        getWgnAutoComplete: function (project_db, partial_name) {
			return $http({
				method: 'GET',
				url: urlBase + 'wgn/autocomplete/' + encodeURIComponent(project_db) + '/' + partial_name
			});
        },
        
        getWgns: function (project_db) {
			return $http({
				method: 'GET',
				url: urlBase + 'wgn/' + encodeURIComponent(project_db)
			});
		},

		getWgnsPage: function (project_db, sortType, sortReverse, page, itemsPerPage) {
			return $http({
				method: 'GET',
				url: urlBase + 'wgn/' + encodeURIComponent(project_db) + '/' + sortType + '/' + sortReverse + '/' + page + '/' + itemsPerPage
			});
		},

        getWgn: function (project_db, id) {
            return $http({
                method: 'GET',
                url: urlBase + 'wgn/' + encodeURIComponent(project_db) + '/' + id
            });
		},
		
		createWgn: function (project_db, data) {
            return $http({
                method: 'POST',
                url: urlBase + 'wgn/' + encodeURIComponent(project_db),
                data: data
            });
        },
		
		updateWgn: function (project_db, id, data) {
            return $http({
                method: 'PUT',
                url: urlBase + 'wgn/' + encodeURIComponent(project_db) + '/' + id,
                data: data
            });
		},
		
		deleteWgn: function (project_db, id) {
            return $http({
                method: 'DELETE',
                url: urlBase + 'wgn/' + encodeURIComponent(project_db) + '/' + id
            });
        },
		
		updateWgnDb: function (project_db, data) {
            return $http({
                method: 'PUT',
                url: urlBase + 'wgn/db/' + encodeURIComponent(project_db),
                data: data
            });
        },

        getWgnMonths: function (project_db, wgn_id) {
			return $http({
				method: 'GET',
				url: urlBase + 'wgn/months/' + encodeURIComponent(project_db) + '/' + wgn_id
			});
		},

        getWgnMonth: function (project_db, id) {
            return $http({
                method: 'GET',
                url: urlBase + 'wgn/month/' + encodeURIComponent(project_db) + '/' + id
            });
		},
		
		createWgnMonth: function (project_db, wgn_id, data) {
            return $http({
                method: 'POST',
                url: urlBase + 'wgn/months/' + encodeURIComponent(project_db) + '/' + wgn_id,
                data: data
            });
        },
		
		updateWgnMonth: function (project_db, id, data) {
            return $http({
                method: 'PUT',
                url: urlBase + 'wgn/month/' + encodeURIComponent(project_db) + '/' + id,
                data: data
            });
		},
		
		deleteWgnMonth: function (project_db, id) {
            return $http({
                method: 'DELETE',
                url: urlBase + 'wgn/month/' + encodeURIComponent(project_db) + '/' + id
            });
        },
    };

}]);