angular.module('swatApp.simulation.service', [])
.factory('simulation', ['$http', function ($http) {
    var urlBase = 'http://localhost:5000/sim/';

    return {
        updateTime: function (project_db, data) {
            return $http({
                method: 'PUT',
                url: urlBase + 'time/' + encodeURIComponent(project_db),
                data: data
            });
        },

        getTime: function (project_db) {
            return $http({
                method: 'GET',
                url: urlBase + 'time/' + encodeURIComponent(project_db)
            });
        },
    };

}]);